import React from 'react';

import { ICharacter, EGender } from '../../__data__/constants/model';
import { male, female } from '../../media'
import { GenderImage, Wrapper, CharImage, TextBlock, Text } from './styled';

/**
 * @prop {ICharacter} char Персонаж.
 */
interface CharacterProps {
    char: ICharacter;
}

/**
 * Иконки для обозначения пола персонажа.
 */
const GenderIcons = {
    [EGender.male]: <GenderImage src={male} />,
    [EGender.female]: <GenderImage src={female} />
}

// Персонаж.
export const Character = ({ char }: CharacterProps) => (
    <Wrapper>
        <CharImage src={char.image} isDead={char.status.toLowerCase() === 'dead'} />
        {GenderIcons[char.gender]}
        <TextBlock>
            <Text>Name:</Text>
            <Text>{char.name}</Text>
        </TextBlock>
        <TextBlock>
            <Text>Status:</Text>
            <Text>{char.status}</Text>
        </TextBlock>
        <TextBlock>
            <Text>Species:</Text>
            <Text>{char.species}</Text>
        </TextBlock>
        <TextBlock>
            <Text>Location:</Text>
            <Text>{char.location.name}</Text>
        </TextBlock>
    </Wrapper>
);
