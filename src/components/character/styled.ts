import styled from 'styled-components';

export const Wrapper = styled.li`
    list-style-type: none;
    padding: 10px;
    margin: 10px;
    background-color: #2c5650;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
    border-radius: 3px;
    min-width: 312px;
`;

interface MortalProps {
    isDead: boolean;
}

export const CharImage = styled.img`
    min-height: 300px;
    min-width: 300px;
    background-color: #294c47;
    ${({ isDead }: MortalProps) => isDead ? 'filter: sepia(1)' : ''};
`;

export const GenderImage = styled.img`
    position: absolute;
    top: 246px;
    right: 16px;
`;

export const Text = styled.span`
    color: #fff;
`;

export const TextBlock = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 10px;
    width: calc(100% - 20px);
`;
