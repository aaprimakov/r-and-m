import React from 'react';
import _ from 'lodash';

/**
 * @prop {number} [offset] Расстояние от экрана, на котором требуется выполнить действие.
 * @prop {boolean} [once] Определяет нужно ли выполнить действие только при первом появлении на экране.
 * @prop {string} [className] Класс.
 * @prop {Function} action Действие, которое необходимо запустить при появлении на экране.
 */
interface IProps {
    offset?: number;
    once?: boolean;
    className?: string;
    action: () => void;
}

/**
 * @prop {boolean} fired Признак срабатывал ли запуск действия.
 * @prop {boolean} isOnScreen Признак нахождения на экране.
 * @prop {number} screenInnerHeight Высота экрана.
 */
interface IState {
    fired: boolean;
    isOnScreen: boolean;
    screenInnerHeight: number;
}

/**
 * Компонент, выполняющий действие, при условии что компонент появился на экране.
 */
export class TrackScrolledTo extends React.PureComponent<IProps, IState> {
    static defaultProps = {
        offset: 0,
        once: false,
    };

    state = {
        fired: false,
        isOnScreen: false,
        screenInnerHeight: 0,
    };

    container: React.RefObject<HTMLDivElement> = React.createRef();

    componentDidMount() {
        const screenInnerHeight = _.get(this.container, 'current.ownerDocument.defaultView.innerHeight', 0);
        this.setState({ screenInnerHeight });
        window.addEventListener('scroll', this.fireActionIfOnScreen);
        this.fireActionIfOnScreen();
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.fireActionIfOnScreen);
    }

    /**
     * Возвращает позицию верхнего края контейнера.
     */
    elTop = () => (this.container && this.container.current && this.container.current.getBoundingClientRect().top) || 0;

    /**
     * Проверяет находится ли компонент на экране.
     * Запускает action если компонент на экране.
     */
    fireActionIfOnScreen = () => {
        const { fired, isOnScreen, screenInnerHeight } = this.state;
        const { once, offset = 0 } = this.props;
        const canIFire = once ? !fired : true;
        const isTimeToFire = screenInnerHeight >= this.elTop() - offset;

        if (canIFire && !isOnScreen && isTimeToFire) {
            this.setState({ fired: true, isOnScreen: true });
            this.props.action();
        } else if (!isTimeToFire && isOnScreen) {
            this.setState({ isOnScreen: false });
        }
    }

    render() {
        const { className } = this.props;

        return (
            <div ref={this.container} className={className}>
                {this.props.children}
            </div>
        );
    }
}
