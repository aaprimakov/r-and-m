import { AnyAction, Dispatch } from 'redux';
import { get } from 'lodash';
import { AxiosError } from 'axios';

import { ASYNC } from "../__data__/constants";
import { IAsyncData, EProcessStatus } from "../__data__/constants/model";

interface IHandlers<S, A> {
    [actionType: string]: (s: S, a: A) => S
}

/**
 * Создает базовый reducer для redux-store.
 *
 * @param {S} initialState Начальное состояние.
 * @param {object} handlers Объект обработчиков.
 */
export const createReducer = <S, A extends AnyAction = AnyAction>(initialState: S, handlers: IHandlers<S, A>) => (state: S = initialState, action: A): S => {
    return Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;
};

/**
 * Создает базовый reducer для асинхронных запросов.
 *
 * @param {string} type Тип экшена.
 */
export const createAsyncHandlers = <R>(type: string) => ({
    [type + ASYNC.BEGIN]: (state: IAsyncData<R>): IAsyncData<R> => ({
        result: state.result,
        status: EProcessStatus.RUNNING,
        error: undefined,
    }),
    [type + ASYNC.SUCCESS]: (state: IAsyncData<R>, action: AnyAction): IAsyncData<R> => ({
        result: get(action, 'payload.data', null),
        status: EProcessStatus.SUCCESS,
        error: undefined,
    }),
    [type + ASYNC.FAILURE]: (state: IAsyncData<R>, action: AnyAction): IAsyncData<R> => ({
        error: action.payload,
        status: EProcessStatus.FAIL,
        result: state.result,
    }),
});

/**
 * Возвращает данные по умолчанию для контейнеров асинхронных запросов.
 */
export const getInitAsyncData = <R>(
    result: R | null = null,
    status: EProcessStatus = EProcessStatus.IDLE,
): IAsyncData<R> => {
        return ({
            result,
            status,
            error: undefined,
        });
    };

/**
 * Асинхронный диспатчер.
 *
 * @param {Dispatch} dispatch Диспатчер.
 * @param {string} actionType Тип экшена.
 * @param {Function} callback Функция обратного вызова.
 */
export function asyncDispatch<T>(dispatch: Dispatch, actionType: string, callback: () => Promise<T>): Promise<T> {
    dispatch({ type: actionType + ASYNC.BEGIN });

    const onFulfilled = (response: T) => {
        dispatch({
            type: actionType + ASYNC.SUCCESS,
            payload: response,
        });

        return Promise.resolve(response);
    };

    const onRejected = (error: AxiosError) => {
        let code = get(error, 'response.status', '');
        let message = get(error, 'response.data.message', 'Не найдено');

        const payload = { code, message };
        const action: AnyAction = {
            type: actionType + ASYNC.FAILURE,
            payload,
        };

        dispatch(action);

        if (!message) {
            console.warn(actionType, error);
        }

        return Promise.reject({
            code,
            message,
            action: action.type,
            error: error.message,
        });
    };

    return callback().then(onFulfilled, onRejected);
}

