declare global {
    interface Window { config: any; }
}

export const getConfigValue = (valueName: string) => window.config[valueName];

