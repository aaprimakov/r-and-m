import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './__data__/reducers'

import MainPage from './pages/main'


const store = createStore(reducer, composeWithDevTools(
    applyMiddleware(),
));

const App = () => (
    <Provider store={store}>
        <Router>
            <Route path="/:name?" component={MainPage} />
        </Router>
    </Provider>
)

export default App;