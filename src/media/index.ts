/// <reference path = './media.d.ts'/>

export { default as logo } from './logo.png';
export { default as sadFace } from './sadFace.png';
export { default as male } from './male.png';
export { default as female } from './female.png';
