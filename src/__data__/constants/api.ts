/**
 * Запрос поиска персонажей по имени.
 *
 * @param {string} name Имя персонажа.
 */
export const searchCharacters = (name: string) => `${window.config.baseApiUrl}/api/character/?name=${name}`;
