// Изменение значения поля поиска.
export const SEARCH_VALUE_CHANGE = 'SEARCH_VALUE_CHANGE';
// Запрс поиска персонажей.
export const SEARCH_REQUEST = 'SEARCH_REQUEST';
// Запрос следующей страницы поиска.
export const NEXT_PAGE = 'NEXT_PAGE';
