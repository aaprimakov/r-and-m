/**
 * Постфиксы для асинхронных экшенов.
 */
export const ASYNC = {
    BEGIN: '_BEGIN',
    SUCCESS: '_SUCCESS',
    FAILURE: '_FAILURE',
};
