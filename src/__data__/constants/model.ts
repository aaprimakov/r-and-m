
/**
 * Общий формат процесса асинхронного запроса.
 *
 * IDLE - Инициализационное состояние, ничего не делается.
 * RUNNING - Идет процесс...
 * SUCCESS - Успешное завершение процесса.
 * FAIL - Процесс завершен с ошибкой.
 */
export enum EProcessStatus {
    IDLE,
    RUNNING,
    SUCCESS,
    FAIL,
}


/**
 * Ошибка возвращаемая сервером.
 *
 * @prop {string} message Сообщение об ошибке.
 * @prop {number} code Код ошибки.
 */
export interface IAsyncError {
    message: string;
    code: number;
}

/**
 * Контейнер асинхронно подгружаемого блока данных.
 *
 * @prop {EProcessStatus} status Статус процесса загрузки данных.
 * @prop {R} result Загруженные данные.
 * @prop {IAsyncError} [error] Ошибка.
 */
export interface IAsyncData<R> {
    status: EProcessStatus;
    result: R | null;
    error?: IAsyncError;
}

export interface ISearchRequestData {
  info: Info;
  results: ICharacter[];
}

export interface ICharacter {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: EGender;
  origin: IOrigin;
  location: IOrigin;
  image: string;
  episode: string[];
  url: string;
  created: string;
}

export enum EGender {
  male = 'Male',
  female = 'Female'
}

interface IOrigin {
  name: string;
  url: string;
}

interface Info {
  count: number;
  pages: number;
  next: string;
  prev: string;
}