import axios from 'axios';
import { Dispatch, AnyAction } from 'redux';

import { SEARCH_VALUE_CHANGE, SEARCH_REQUEST, NEXT_PAGE } from '../constants/actionTypes';
import { asyncDispatch } from '../../utils/reducerUtils';
import { searchCharacters } from '../constants/api';
import { ISearchRequestData } from '../constants/model';

export const changeSearchValue = (dispatch: Dispatch<AnyAction>) => (value: string) => dispatch({
    type: SEARCH_VALUE_CHANGE,
    value
})

export const requestCharacter = (dispatch: Dispatch<AnyAction>) => (name: string) => asyncDispatch(dispatch, SEARCH_REQUEST, () => {
    return axios.get<ISearchRequestData>(searchCharacters(name))
})

export const requestNextPage = (dispatch: Dispatch<AnyAction>) => (url: string) => asyncDispatch(dispatch, NEXT_PAGE, () => {
    return axios.get<ISearchRequestData>(url)
})
