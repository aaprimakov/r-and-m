import _ from "lodash";
import { AnyAction } from "redux";

import { SEARCH_VALUE_CHANGE, SEARCH_REQUEST, NEXT_PAGE } from "../constants/actionTypes";
import { createReducer, getInitAsyncData, createAsyncHandlers } from "../../utils/reducerUtils";
import { IAsyncData, ISearchRequestData } from "../constants/model";
import { ASYNC } from '../constants'

export interface ISearchStore {
    searchValue: string;
    searchRequest: IAsyncData<ISearchRequestData>
}

const initState: ISearchStore = {
    searchValue: '',
    searchRequest: getInitAsyncData(),
};

interface Action extends AnyAction {
    value?: string;
}

const searchValueHandler = (state: string, action: Action) => action.value || ''

const searchValueReducer = createReducer<string>(initState.searchValue, {
    [SEARCH_VALUE_CHANGE]: searchValueHandler
})

const reqReducer = createReducer<IAsyncData<ISearchRequestData>>(initState.searchRequest, {
    ...createAsyncHandlers(SEARCH_REQUEST),
    [NEXT_PAGE + ASYNC.SUCCESS]: (state: IAsyncData<ISearchRequestData>, action: AnyAction) => console.info(action) as any || ({
        ...state,
        result: {
            info: action.payload.data.info,
            results: [
                ..._.get(state.result, 'results', []),
                ...action.payload.data.results
            ]
        }
    })
})

export const searchReducer = (state: ISearchStore = initState, action: AnyAction): ISearchStore => ({
    searchValue: searchValueReducer(state.searchValue, action),
    searchRequest: reqReducer(state.searchRequest, action)
});