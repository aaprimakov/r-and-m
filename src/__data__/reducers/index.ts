import { combineReducers } from 'redux';

import { searchReducer, ISearchStore } from './search';

/**
 * Стор приложения.
 *
 * @prop {ISearchStore} search стор для поиска.
 */
export interface IAppStore {
    search: ISearchStore;
}

export default combineReducers<IAppStore>({
    search: searchReducer,
})
