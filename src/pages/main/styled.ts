import styled from 'styled-components';

const black = '#294b61';
const bSize = 2;

export const Header = styled.h1`
    color: #04b1c8;
    text-shadow: ${bSize}px 0 0 ${black},
        -${bSize}px 0 0 ${black},
        0 ${bSize}px 0 ${black},
        0 -${bSize}px 0 ${black},
        ${bSize/2}px ${bSize/2}px 0 ${black},
        -${bSize/2}px -${bSize/2}px 0 ${black},
        ${bSize/2}px -${bSize/2}px 0 ${black},
        -${bSize/2}px ${bSize/2}px 0 ${black};
`;

export const Wrapper = styled.main`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

export const SearchBox = styled.div`
    padding: 10px;
`;

export const Logo = styled.img`
    width: 450px;
    height: 100%;
    max-width: 100%;
`;

export const ErrorResult = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    img {
        margin-top: 40px;
        max-width: 60%;
    }
`;

export const ResultsBox = styled.ul`
    width: 100%;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    padding: 0;
`;

export const EmptyCardplaceholders = styled.div`
    width: 352px;
`;
