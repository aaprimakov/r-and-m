import React, { SyntheticEvent, ChangeEvent } from 'react';
import classnames from 'classnames';
import { Input, Button, CircularProgress, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { RouteChildrenProps } from 'react-router';
import _ from 'lodash';

import { changeSearchValue, requestCharacter as requestCharAction, requestNextPage } from '../../__data__/actions/search'
import { IAppStore } from '../../__data__/reducers';
import { logo, sadFace } from '../../media'
import { EProcessStatus, IAsyncData, ISearchRequestData } from '../../__data__/constants/model';
import { Character, TrackScrolledTo } from '../../components';
import { getConfigValue } from '../../utils/config';

import { Wrapper, SearchBox, Header, Logo, ResultsBox, ErrorResult, EmptyCardplaceholders } from './styled';

interface IPageProps {}

/**
 * @prop {string} searchValue Строка поиска.
 * @prop {IAsyncData<ISearchRequestData>} searchRequest Информация асинхронного запроса поиска.
 */
interface IPageStateToProps {
    searchValue: string;
    searchRequest: IAsyncData<ISearchRequestData>;
}

/**
 * @prop {Function} changeSearchValue Функция изменения поисковой строки.
 * @prop {Function} requestNextPage Функция подгрузки данных в ленту.
 * @prop {Function} requestCharacter Функция запроса поисковой выдачи.
 */
interface IPageDispatchToProps {
    changeSearchValue: (value: string) => void;
    requestNextPage: (url: string) => void;
    requestCharacter: (name: string) => void;
}

type TPageProps = IPageProps & IPageStateToProps & IPageDispatchToProps & RouteChildrenProps<{ name: string }>;

// Страница поиска персонажей.
class Page extends React.Component<TPageProps, {}> {
    inputRef: React.RefObject<HTMLInputElement> = React.createRef();
    autosearchTimerId?: number;

    componentDidMount() {
        this.inputRef.current && this.inputRef.current.focus();
        const { match, requestCharacter, changeSearchValue } = this.props;

        if (match && match.params.name) {
            changeSearchValue(match.params.name);
            requestCharacter(match.params.name)
        }
    }

    /**
     * Обработчик запроса доролнительной выдачи.
     */
    handleGetNextPage = () => {
        const { searchRequest, requestNextPage } = this.props;

        const nextPageUrl = _.get(searchRequest, 'result.info.next');

        if (nextPageUrl) {
            requestNextPage(nextPageUrl);
        }
    }

    /**
     * Обработчик отправки формы.
     *
     * @prop {SyntheticEvent<HTMLFormElement>} event Событие.
     */
    handleSubmit = (event?: SyntheticEvent<HTMLFormElement>) => {
        event && event.preventDefault();
        const { searchValue, requestCharacter, history } = this.props;
        history.push(`/${searchValue}`)

        requestCharacter(searchValue);
    }

    /**
     * Обработчик изменения в строке поиска.
     *
     * @prop {ChangeEvent<HTMLInputElement>} event Событие.
     */
    handleSearchChange = (event: ChangeEvent<HTMLInputElement>) => {
        const value = event.currentTarget.value;

        this.props.changeSearchValue(value);


        window.clearTimeout(this.autosearchTimerId)
        this.autosearchTimerId = window.setTimeout(this.handleSubmit, 1000);
    }

    componentWillUnmount() {
        window.clearTimeout(this.autosearchTimerId)
    }

    /**
     * Рендер списка персонажей.
     */
    renderCharacters = () => {
        const { searchRequest } = this.props;

        if (searchRequest.status === EProcessStatus.RUNNING) {
            return <CircularProgress color="secondary" />;
        }

        if (searchRequest.status === EProcessStatus.FAIL) {
            return (
                <ErrorResult>
                    <Typography color="secondary">{searchRequest.error && searchRequest.error.message}</Typography>
                    <img alt="not found" src={sadFace} />
                </ErrorResult>
            )
        }

        if (searchRequest.status !== EProcessStatus.SUCCESS) {
            return null;
        }

        return searchRequest.result && searchRequest.result.results
            .map(character => <Character char={character} key={character.id} />)
    }

    render() {
        const { searchValue, searchRequest } = this.props;
        const isLoading = searchRequest.status === EProcessStatus.RUNNING
        const nextPageExist = Boolean(_.get(searchRequest, 'result.info.next'));

        return (
            <Wrapper>
                <Logo src={logo} />    
                <Header>Characters search</Header>
                <SearchBox>
                    <form onSubmit={this.handleSubmit}>
                        <Input
                            className={classnames({ 'search-input': getConfigValue('testing') })}
                            inputRef={this.inputRef}
                            value={searchValue}
                            onChange={this.handleSearchChange}
                        />
                        <Button type="submit">Search</Button>
                    </form>
                </SearchBox>
                <ResultsBox>
                    {this.renderCharacters()}
                    {_.get(searchRequest, 'result.results.length', 0) > 3 && <>
                        <EmptyCardplaceholders />
                        <EmptyCardplaceholders />
                        <EmptyCardplaceholders />
                    </>}
                </ResultsBox>
                {!isLoading && nextPageExist && (
                    <TrackScrolledTo action={this.handleGetNextPage} offset={500}>
                        <CircularProgress color="secondary" />
                    </TrackScrolledTo>
                )}
            </Wrapper>
        )
    }
}

const mapStateToProps = (state: IAppStore): IPageStateToProps => ({
    searchValue: state.search.searchValue,
    searchRequest: state.search.searchRequest
})

const mapDispatchToProps = (dispatch: any): IPageDispatchToProps => ({
    changeSearchValue: changeSearchValue(dispatch),
    requestNextPage: requestNextPage(dispatch),
    requestCharacter: requestCharAction(dispatch)
})

export default connect<IPageStateToProps, IPageDispatchToProps, {}, IAppStore>(mapStateToProps, mapDispatchToProps)(Page);
