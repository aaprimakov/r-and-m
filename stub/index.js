const express = require('express');
const path = require('path');

const app = express()
const port = process.env.PORT || 3001

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
    next();
});

app.use(express.static(path.resolve(__dirname, '..', 'dist')));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.get('/api/character/', (req, res) => {
    const ricks = require('./mocks/ricks');
    res.send(ricks);
})

app.get('/', (req, res) => res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html')))
