const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const isProd = (enviroment) => enviroment.mode === 'production'

module.exports = (_, env) => ({
    entry: path.resolve(__dirname, 'src', 'index.tsx'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/'
    },

    resolve: {
        extensions: ['.wasm', '.mjs', '.js', '.json', '.ts', '.tsx']
    },

    module: {
        rules: [{
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            },

            {
                test: /\.(png|jpe?g|gif)$/i,
                use: {
                    loader: 'file-loader',
                },
            },
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public', 'index.ejs'),
            minify: true,
            favicon: path.resolve(__dirname, 'public', 'favicon.ico'),
            templateParameters: {
                apiUrl: (isProd(env) && !process.env.TESTING) ? 'https://rickandmortyapi.com' : 'http://localhost:8081',
                testing: process.env.TESTING ? true : false
            }
        }),
    ],

    devServer: {
        port: env.PORT || 8080,
        historyApiFallback: true
    },
});
