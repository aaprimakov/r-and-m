describe('Simple testing', function() {
    it('just works', function () {
        expect(true).to.equal(true);

        cy.visit('/');

        cy.get('ul li').should('have.length', 0)

        cy.get('.search-input')
            .type('rick', { delay: 50 })
            .get('input')
            .should('have.value', 'rick');

        cy.get('ul li').should('have.length', 20);
    });
});

